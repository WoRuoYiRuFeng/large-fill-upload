# large-fill-upload

## 项目初始化

下载项目
```bash
git clone https://gitee.com/WoRuoYiRuFeng/large-fill-upload
```

进入项目并下载依赖
```bash
cd large-fill-upload && npm i
```

启动服务端
```bash
node ./server/http.js
```

启动前端
```bash
npm run serve
```



## 项目介绍

![image-20210507115130762](https://tva1.sinaimg.cn/large/008i3skNly1gq9q9sfzjhj30x70i4goh.jpg)


## 参考文件

https://juejin.cn/post/6844904046436843527#heading-3

