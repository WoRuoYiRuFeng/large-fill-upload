/*
 * @Author: your name
 * @Date: 2021-04-30 13:38:15
 * @LastEditTime: 2021-04-30 13:38:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /large-fill-upload/src/utils/request.js
 */

export function request({
    url,
    method = "post",
    data,
    headers = {},
    requestList
}) {
    return new Promise(resolve => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url);
        Object.keys(headers).forEach(key =>
            xhr.setRequestHeader(key, headers[key])
        );
        xhr.send(data);
        xhr.onload = e => {
            resolve({
                data: e.target.response
            });
        };
    });
}