import Vue from 'vue'
import Router from 'vue-router'
// import utils from './modules/utils'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [{
    path: '/',
    redirect: '/dashboard',
    component: Layout,
    meta: {
      title: '首页'
    },
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/fileUpload/index'),
      meta: {
        title: '首页',
        icon: 'dashboard',
        affix: true
      }
    }, ]
  },
  // ...utils
]
var names = []

// 获取路由名称
function getName(children) {
  children.forEach(item => {
    names.push(item.name)
    if (item.children && item.children.length > 0) {
      getName(item.children)
    }
  })
};
getName(constantRoutes)
// 抛出路由名称
export const constantNames = names


const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router