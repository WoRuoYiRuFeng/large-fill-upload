import Layout from '@/layout'

export default {
  path: '/utils/:id',
  name: 'utils',
  component: Layout,
  hidden: true,
  meta: {
    tag: false
  },
  children: [{
    path: 'success',
    name: 'utils-success',
    component: () => import('@/views/utils/Success'),
    meta: {
      title: '操作成功',
      tag: false
    },
    hidden: true
  }]
}
